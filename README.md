# OpenML dataset: lupus

https://www.openml.org/d/472

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

87 persons with lupus nephritis. Followed up 15+ years. 35 deaths. Var =
duration of disease. Over 40 baseline variables avaiable from authors.
Description :
For description of this data set arising from 87 persons
with lupus nephritis followed for 15+ years after an initial
renal biopsy (the starting point of follow-up) see the introduction to
Abrahamowicz, MacKenzie and Esdaile (December 1996 issue).
This data set only contains time to death/censoring, indicator,
duration and log(1+duration), where duration is the duration
of untreated  disease prior to biopsy. This variable was the
focus in the aforementioned JASA article because it clearly
violates the proportionality of hazards assumption. More than
40 other variables acquired at baseline are available from
authors.
Permission :
This data can be freely used for non-commercial purposes and
distributed freely.
Michal Abrahamowicz, Todd MacKenzie and John Esdaile


Information about the dataset
CLASSTYPE: nominal
CLASSINDEX: 2

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/472) of an [OpenML dataset](https://www.openml.org/d/472). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/472/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/472/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/472/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

